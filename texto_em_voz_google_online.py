#!/usr/bin/env python3.11

import sys
import os
import argparse
from gtts import gTTS

def text_to_speech(text, lang='pt-br', speed=1.0, save=False, filename='output.mp3'):
    tts = gTTS(text, lang=lang)
    tts.speed = speed

    if save:
        tts.save(filename)
    else:
        tts.save('output_temp.mp3')
        os.system('mpg123 -q output_temp.mp3')
        os.remove('output_temp.mp3')

def main():
    parser = argparse.ArgumentParser(description="Converte um arquivo de texto em áudio utilizando a biblioteca gTTS",
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("--lang", default="pt-br", help="Idioma da fala")
    parser.add_argument("--file", required=True, help="Caminho para o arquivo de texto")
    parser.add_argument("--speed", type=float, default=1.0, help="Velocidade da fala")
    parser.add_argument("--save", action="store_true", help="Salvar o conteúdo em áudio em vez de reproduzi-lo")
    parser.add_argument("--filename", default='output.mp3', help="Nome do arquivo de saída quando usando --save")

    args = parser.parse_args()

    file_path = args.file
    lang = args.lang
    speed = args.speed
    save = args.save
    filename = args.filename

    try:
        with open(file_path, 'r') as file:
            text = file.read()
            text_to_speech(text, lang, speed, save, filename)
    except FileNotFoundError:
        print("Arquivo não encontrado.")
    except:
        print("Ocorreu um erro ao ler o arquivo.")

if __name__ == '__main__':
    main()
